//
//  DashBoardViewController.swift
//  MOWO
//
//  Created by Adisorn Chatnaratanakun on 2/23/2559 BE.
//  Copyright © 2559 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit
import Charts

class DashBoardViewController: UITableViewController {
    
    @IBOutlet weak var pieChartView: PieChartView!
    
    var toPass = Dictionary<String,String>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        
        // Do any additional setup after loading the view.
        
        let perCon = Double(toPass["ComPercent"]!)
        let InCon = Double(toPass["InComPercent"]!)
        
        let TaskStatus = ["Completed Task", "Incompleted Task"]
        let Percent = [Double(perCon!), Double(InCon!)]
        
        setChart(TaskStatus, values: Percent)
        
        
    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(value: values[i], xIndex: i)
            dataEntries.append(dataEntry)
        }
        
        let pieChartDataSet = PieChartDataSet(yVals: dataEntries, label: "")
        
        var colors: [UIColor] = []
        
        for i in 0..<dataPoints.count {
            let red = Double(arc4random_uniform(256))
            let green = Double(arc4random_uniform(256))
            let blue = Double(arc4random_uniform(256))
            
            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
            colors.append(color)
        }
        
        pieChartDataSet.colors = colors
        
        
        let pieChartData = PieChartData(xVals: dataPoints, dataSet: pieChartDataSet)
        pieChartView.data = pieChartData
    }
    
}
