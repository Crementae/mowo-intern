//
//  ProfileViewController.swift
//  MOWO
//
//  Created by Adisorn Chatnaratanakun on 2/23/2559 BE.
//  Copyright © 2559 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {
    
    @IBOutlet weak var ProfileImage: UIImageView!
    
    @IBOutlet weak var CoverImage: UIImageView!
    
    
    var Prodict = ["name":"Adisorn Chatnaratanakun", "email": "cemgyjghe@gmail.com", "empID": "ID_123123678123","role": "member"]
    
    var TaskStatus = ["Completed": "Completed Task","ComPercent": "70.0" ,"Incompleted": "Incompleted Task","InComPercent": "30.0"]
    
    
    var TravelDistances = ["TravelDistances": "100", "hours":"22"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //tableView.backgroundView = UIImageView(image: UIImage(named: "Copy of background"))
        //self.navigationController!.navigationBar.barTintColor = UIColor.blackColor()
        
        
        let ProfileNib = UINib(nibName: "ProfileCell", bundle: nil)
        tableView.registerNib(ProfileNib, forCellReuseIdentifier: "ProCell")
        
        let TaskStatusNib = UINib(nibName: "TaskStatusCell", bundle: nil)
        tableView.registerNib(TaskStatusNib, forCellReuseIdentifier: "TaskCell")
        
        let TravelDistancesNib = UINib(nibName: "TravelDistancesCell", bundle: nil)
        tableView.registerNib(TravelDistancesNib, forCellReuseIdentifier: "TravelCell")
        
        //set table view background color to clear
        //tableView.backgroundColor = UIColor.clearColor();
        
        //Set Profile Image
        
        ProfileImage.image = UIImage(named: "tae.jpg")
        
        
        
        //Set Profile Image to circular image
        ProfileImage.layer.borderWidth=2.0
        ProfileImage.layer.masksToBounds = false
        ProfileImage.layer.borderColor = UIColor.whiteColor().CGColor
        ProfileImage.layer.cornerRadius = 13
        ProfileImage.layer.cornerRadius = ProfileImage.frame.size.height/2
        ProfileImage.clipsToBounds = true
        
        
        //Set Cover Image
        CoverImage.image = UIImage(named: "Phu.jpg")
        
        
        
        
    }
    
    
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        if indexPath.section == 0{
            
            let cell:ProfileCellTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("ProCell") as! ProfileCellTableViewCell
            
            
            //cell.layer.cornerRadius = 10.0
            cell.contentView.layer.masksToBounds = true
            cell.Name.text = Prodict["name"]?.uppercaseString
            cell.Email.text = Prodict["email"]
            cell.EmpID.text = Prodict["empID"]
            cell.Role.text = Prodict["role"]
            cell.ProfileIcon.image = UIImage(named: "ProfileIcon")!
            
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            
            
            
            print  ("Section \(indexPath.section) Row \(indexPath.row)")
            
            return cell
            
        }else if indexPath.section == 1{
            
            let cell:StatusTaskTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("TaskCell") as! StatusTaskTableViewCell
            
            //cell.layer.cornerRadius = 10.0
            
            cell.contentView.layer.masksToBounds = true
            /*
            
            cell.CompletedTask.text = TaskStatus["Completed"]
            
            cell.IncompletedTask.text = TaskStatus["Incompleted"]
            
            cell.TaskIcon.image = UIImage(named: "TaskStatus")
            
            cell.ComPercent.text = TaskStatus["ComPercent"]!+"%"
            
            cell.InComPercent.text = TaskStatus["InComPercent"]!+"%"
            */
            
            
            
            return cell
            
        }else{
            
            
            let cell:TravelDistancesTableViewCell = self.tableView.dequeueReusableCellWithIdentifier("TravelCell") as! TravelDistancesTableViewCell
            
            //cell.layer.cornerRadius = 10.0
            
            cell.contentView.layer.masksToBounds = true
            
            cell.TravelDistances.text = TravelDistances["TravelDistances"]!+" KM IN "+TravelDistances["hours"]!+" HOURS"
            
            return cell
        }
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 1 {
            self.performSegueWithIdentifier("showDashboard", sender: self)
        } else if indexPath.section == 2 {
            self.performSegueWithIdentifier("showTravel", sender: self)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showDashboard" {
            
            let DBV =   segue.destinationViewController as! DashBoardViewController
            
            DBV.toPass = TaskStatus
            
        } else if segue.identifier == "showTravel" {
            
            let TVC = segue.destinationViewController as! TimelineViewController
            
            TVC.travelPass = TravelDistances
            
        }
        
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(table1iew: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    //    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
    //
    //        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "ProCell")
    //
    //       cell.imageView?.image = UIImage(named: "ProIcon")
    //
    //        return cell
    //    }
    
    
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
    
    // Configure the cell...
    
    return cell
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
