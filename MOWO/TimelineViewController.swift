//
//  TravelViewController.swift
//  MOWO
//
//  Created by Adisorn Chatnaratanakun on 2/23/2559 BE.
//  Copyright © 2559 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit

class TimelineViewController: UIViewController {
    

    
    //Navagate to map view
    @IBAction func MapSegue(sender: AnyObject) {
        
        let mapViewControllerObejct = self.storyboard?.instantiateViewControllerWithIdentifier("MapView") as? MapViewController
        
        self.navigationController?.pushViewController(mapViewControllerObejct!, animated: true)

        
    }
    //Back
    @IBAction func Back(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
        
        
    }
    
    
    var travelPass = Dictionary<String,String>()
    
    var scrollView: UIScrollView!
    var timeline:   TimelineView!

    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        
      
  
        view.addConstraints([
            NSLayoutConstraint(item: scrollView, attribute: .Left, relatedBy: .Equal, toItem: view, attribute: .Left, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: scrollView, attribute: .Top, relatedBy: .Equal, toItem: view, attribute: .Top, multiplier: 1.0, constant: 110),
            NSLayoutConstraint(item: scrollView, attribute: .Right, relatedBy: .Equal, toItem: view, attribute: .Right, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: scrollView, attribute: .Bottom, relatedBy: .Equal, toItem: view, attribute: .Bottom, multiplier: 1.0, constant: 0)
            ])
        
        timeline = TimelineView(bulletType: .Circle, timeFrames: [
            TimeFrame(time: "9.00", location: "GoPomelo", image: UIImage(named: "fireworks.jpeg")),
            TimeFrame(time: "11.00", location: "Home", image: UIImage(named: "heart.png")),
            TimeFrame(time: "13.00", location:  "School",  image: nil),
            TimeFrame(time: "14.00", location: "University", image: UIImage(named: "april.jpeg")),
            TimeFrame(time: "15.00", location: "The mall", image: nil),
            TimeFrame(time: "16.00", location: "SIAM", image: nil),
            TimeFrame(time: "17.00", location:  "PARAGON", image: nil)
            
            
            ])
        
        //Set TOTAL WAYPOINT, TOTAL DISTANCES and TOTAL TIME
        if let titleWaypoint = self.view.viewWithTag(110) as? UILabel {
            
            titleWaypoint.lineBreakMode = NSLineBreakMode.ByWordWrapping
            titleWaypoint.numberOfLines = 3
            
            
            
            titleWaypoint.text = "WAYPOINTS"
            
        }
        if let totalWaypoint = self.view.viewWithTag(120) as? UILabel {
            
            
            let itemCount = timeline.timeFrames.count
            
            totalWaypoint.text = String(itemCount)
            
        }
        if let titleDistance = self.view.viewWithTag(210) as? UILabel {
            
            
            titleDistance.lineBreakMode = NSLineBreakMode.ByWordWrapping
            titleDistance.numberOfLines = 3
            
            
            
            titleDistance.text = "KILOMETERS"
            
        }
        if let totalDistance = self.view.viewWithTag(220) as? UILabel {
            
            
            totalDistance.lineBreakMode = NSLineBreakMode.ByWordWrapping
            totalDistance.numberOfLines = 3
            
            
            
            totalDistance.text = "100"
            
        }
        if let titleTime = self.view.viewWithTag(310) as? UILabel {
            
            
            titleTime.lineBreakMode = NSLineBreakMode.ByWordWrapping
            titleTime.numberOfLines = 3
            
            
            
            titleTime.text = "HOURS"
            
        }
        if let totalTime = self.view.viewWithTag(320) as? UILabel {
            
            
            totalTime.lineBreakMode = NSLineBreakMode.ByWordWrapping
            totalTime.numberOfLines = 3
            
            
            
            totalTime.text = "22"
            
        }
        
        
    
    
    
        
        let time = timeline.timeFrames[0]
        
        
        print(time)
        
        
        scrollView.addSubview(timeline)
        scrollView.addConstraints([
            NSLayoutConstraint(item: timeline, attribute: .Left, relatedBy: .Equal, toItem: scrollView, attribute: .Left, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: timeline, attribute: .Bottom, relatedBy: .LessThanOrEqual, toItem: scrollView, attribute: .Bottom, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: timeline, attribute: .Top, relatedBy: .Equal, toItem: scrollView, attribute: .Top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: timeline, attribute: .Right, relatedBy: .Equal, toItem: scrollView, attribute: .Right, multiplier: 1.0, constant: 0),
            
            NSLayoutConstraint(item: timeline, attribute: .Width, relatedBy: .Equal, toItem: scrollView, attribute: .Width, multiplier: 1.0, constant: 0)
            ])
        
        view.sendSubviewToBack(scrollView)
    }
    
// Segue with data
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if segue.identifier == "MapTravel"{
//            var DestViewController = segue.destinationViewController as! UINavigationController
//            let targetController = DestViewController.topViewController as! MapViewController
//            print("Success")
//            
//        }
//    }
    
    @IBAction func bulletChanged(sender: UISegmentedControl) {
        timeline.bulletType = [BulletType.Circle, BulletType.Hexagon, BulletType.Diamond, BulletType.DiamondSlash, BulletType.Carrot, BulletType.Arrow][sender.selectedSegmentIndex]
    }
    
//    override func prefersStatusBarHidden() -> Bool {
//        return true
//    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated);
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
}
