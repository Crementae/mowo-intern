//
//  MapViewController.swift
//  MOWO
//
//  Created by Adisorn Chatnaratanakun on 2/29/2559 BE.
//  Copyright © 2559 Adisorn Chatnaratanakun. All rights reserved.
//


import UIKit
import GoogleMaps

class MapViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.cameraWithLatitude(-33.868,
            longitude:151.2086, zoom:6)
        let mapView = GMSMapView.mapWithFrame(CGRectZero, camera:camera)
        
        let coord1 = CLLocation(latitude: -33.866, longitude: 151.195)
        let point1 = coord1.coordinate
        
        let marker = GMSMarker()
        marker.position = point1
        marker.snippet = "Hello World"
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.map = mapView
        
        let coord2 = CLLocation(latitude: -33.860, longitude: 148.001)
        let point2 = coord2.coordinate
        
        let marker2 = GMSMarker()
        marker2.position = point2
        marker2.snippet = "asdasd"
        marker2.appearAnimation = kGMSMarkerAnimationPop
        marker2.map = mapView
        
     
        
        let distance = coord1.distanceFromLocation(coord2)
        
        print("the distance between the two points is: \(distance) KM")
        
        let path = GMSMutablePath()
        path.addLatitude(-33.866, longitude:151.195) // Sydney
        path.addLatitude(-33.860, longitude:148.001) // Fiji
//        path.addLatitude(21.291, longitude:-157.821) // Hawaii
//        path.addLatitude(37.423, longitude:-122.091) // Mountain View
        
        
        
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor.blueColor()
        polyline.strokeWidth = 1.0
        polyline.map = mapView
        
        self.view = mapView
        
    }
}