//
//  StatusTaskTableViewCell.swift
//  Mowo
//
//  Created by Adisorn Chatnaratanakun on 2/16/2559 BE.
//  Copyright © 2559 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit

class StatusTaskTableViewCell: UITableViewCell {

    @IBOutlet weak var CompletedTask: UILabel!
    
    @IBOutlet weak var IncompletedTask: UILabel!
    
    @IBOutlet weak var TaskIcon: UIImageView!
    
    @IBOutlet weak var ComPercent: UILabel!
    
    @IBOutlet weak var InComPercent: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
