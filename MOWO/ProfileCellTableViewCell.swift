//
//  ProfileCellTableViewCell.swift
//  Mowo
//
//  Created by Adisorn Chatnaratanakun on 2/15/2559 BE.
//  Copyright © 2559 Adisorn Chatnaratanakun. All rights reserved.
//

import UIKit

class ProfileCellTableViewCell: UITableViewCell {

    @IBOutlet weak var ProfileIcon: UIImageView!
    
    @IBOutlet weak var Name: UILabel!
    
    @IBOutlet weak var Email: UILabel!
    @IBOutlet weak var EmpID: UILabel!
    @IBOutlet weak var Role: UILabel!
    
    
    

    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
